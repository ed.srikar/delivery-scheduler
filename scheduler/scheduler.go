package scheduler

import "delivery-scheduler/entities"

type DeliveryScheduler interface {
	GetSchedule(network entities.Network, vehicles []*entities.Vehicle, packages []*entities.Package)
}
