package entities

type VehicleType string

const (
	Train VehicleType = "Train"
)
