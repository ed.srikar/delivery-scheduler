package entities

type Package struct {
	Name       string
	StartPoint *Location
	EndPoint   *Location
	Distance   int
}