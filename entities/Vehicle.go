package entities

type Vehicle struct {
	Name            string
	TypeOfVehicle   VehicleType
	Capacity        int
	FilledCapacity int
	CurrentLocation *Location
}
