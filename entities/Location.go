package entities

import "fmt"

type Location struct {
	Name  string
	Paths []*Path
}

func (l *Location) AddPath(path *Path) error {
	if path.LocationStart != nil && path.LocationStart.Name != l.Name {
		return fmt.Errorf("start location doesnt match")
	}

	for index, existingPath := range l.Paths {
		if existingPath.LocationStart == path.LocationEnd {
			l.Paths[index].Distance = path.Distance
			return nil
		}
	}
	l.Paths = append(l.Paths, path)
	return nil
}
