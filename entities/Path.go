package entities

type Path struct {
	Name          string
	LocationStart *Location
	LocationEnd   *Location
	Distance      int
}
