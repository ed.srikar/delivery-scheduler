package entities

import "fmt"

type Network struct {
	Locations map[string]*Location
}

//func (n *Network) GetDistance(locationA string, locationB string) int {
//	locA := n.Locations[locationA]
//	locB := n.Locations[locationB]
//
//	isVisitedGraph := map[string]bool{locationA:true}
//
//	paths := []Path{}
//	currDistance := 0
//
//
//
//}

func (n *Network) AddLocation(location *Location) error {
	if location.Name == "" {
		return fmt.Errorf("location Name cannot be empty")
	}
	n.Locations[location.Name] = location
	return nil
}

func (n *Network) AddPath(path *Path) {
	for _, location := range n.Locations {
		if location.Name == path.LocationStart.Name {
			location.AddPath(path)
		}
		if location.Name == path.LocationEnd.Name {
			location.AddPath(&Path{
				LocationStart: path.LocationEnd,
				LocationEnd:   path.LocationStart,
				Distance:      path.Distance,
				Name:          path.Name,
			})
		}
	}
}

func (n *Network) GetLocation(name string) (*Location, error) {
	location, exists := n.Locations[name]
	if !exists {
		return nil, fmt.Errorf("location unavailable")
	}
	return location, nil
}
