package main

import (
	"delivery-scheduler/server"
)

func main() {
	server.StartServer()
}
