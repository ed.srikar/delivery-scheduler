# Delivery Scheduler
The following software can be used for scheduling a package delivery.
Given a set of packages (with weights, starting points and destinations)
, different delivery vehicles (with capacity and starting points)
and a set of routes (with time to cover them) connecting different locations,
the software generates a delivery schedule (based on a predefined algorithm)
describing the way packages can be moved to their destinations.
There may be situations when a given schedule cannot be in followed.In that situation the
software can be configured with a different algorithm and it will accordingly
generate a new delivery schedule based on the new algorithm.

# Run

1. Install go
2. Use the command 
   ```sh
    go run main.go
    ```
3. Enter the details of locations, packages and paths. Sample copy paste the below
   ```sh
    3
    A
    B
    C
    2
    E1,A,B,3
    E2,B,C,1
    1
    P1,A,C,5
    1
    Q1,B,6
    ```