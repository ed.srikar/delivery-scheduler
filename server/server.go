package server

import "delivery-scheduler/entities"

var Network *entities.Network

func InitNetwork() {
	if Network == nil {
		Network = &entities.Network{
			Locations: map[string]*entities.Location{},
		}
	}
}

func StartServer() {
	InitConfig()
	InitNetwork()
	InitInput()
	for true {
		GenerateNetwork()
		PrintNetwork()
	}
}
