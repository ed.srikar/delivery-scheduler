package server

import (
	"bufio"
	"delivery-scheduler/entities"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type InputInterface interface {
	GetTrains() ([]*entities.Vehicle, error)
	GetLocations() ([]*entities.Location, error)
	GetPaths() ([]*entities.Path, error)
	GetPackages() ([]*entities.Package, error)
}

var Input *CMD

func InitInput() {
	if Input == nil {
		if Config.ServerType == "cmd" {
			Input = &CMD{}
		} else {
			Input = &CMD{}
		}
	}
}

func PrintNetwork() {
	for name, location := range Network.Locations {
		fmt.Println(name)
		for _, path := range location.Paths {
			fmt.Print(path.Name, ",", path.LocationStart.Name, ",", path.LocationEnd.Name, ",", path.Distance)
			fmt.Println()
		}
		fmt.Println()
		fmt.Println()
		fmt.Println()
	}
}

func GenerateNetwork() ([]*entities.Location, []*entities.Path, []*entities.Package, []*entities.Vehicle, error) {
	locations, err := Input.GetLocations()
	if err != nil {
		return nil, nil, nil, nil, err
	}
	for _, location := range locations {
		Network.AddLocation(location)
	}

	paths, err := Input.GetPaths()
	if err != nil {
		return nil, nil, nil, nil, err
	}
	for _, path := range paths {
		Network.AddPath(path)
	}

	packages, err := Input.GetPackages()
	if err != nil {
		return nil, nil, nil, nil, err
	}

	trains, err := Input.GetTrains()
	if err != nil {
		return nil, nil, nil, nil, err
	}
	return locations, paths, packages, trains, nil
}

type CMD struct {
}

func (c *CMD) PrintNetwork() {
}

func (c *CMD) GetTrains() ([]*entities.Vehicle, error) {
	reader := bufio.NewReader(os.Stdin)
	numOfTrains, err := getNumber(reader)
	if err != nil {
		return nil, err
	}

	var trains []*entities.Vehicle
	for i := 0; i < numOfTrains; i++ {
		trainParts, err := getDelimetedStrings(reader, 3)
		if err != nil {
			return nil, err
		}

		currLocation, err := Network.GetLocation(trainParts[1])
		if err != nil {
			return nil, err
		}

		capacity, err := strconv.Atoi(trainParts[2])
		if err != nil {
			return nil, err
		}

		trains = append(trains, &entities.Vehicle{
			Name:            trainParts[0],
			TypeOfVehicle:   entities.Train,
			Capacity:        capacity,
			CurrentLocation: currLocation,
		})
	}
	return trains, nil
}

func (c *CMD) GetLocations() ([]*entities.Location, error) {
	reader := bufio.NewReader(os.Stdin)
	numOfLocations, err := getNumber(reader)
	if err != nil {
		return nil, err
	}

	var locations []*entities.Location
	for i := 0; i < numOfLocations; i++ {
		locationParts, err := getDelimetedStrings(reader, 1)
		if err != nil {
			return nil, err
		}
		locations = append(locations, &entities.Location{
			Name: locationParts[0],
		})
	}

	return locations, nil
}

func (c *CMD) GetPackages() ([]*entities.Package, error) {
	reader := bufio.NewReader(os.Stdin)
	numOfPackages, err := getNumber(reader)
	if err != nil {
		return nil, err
	}

	var packages []*entities.Package

	for i := 0; i < numOfPackages; i++ {
		packageParts, err := getDelimetedStrings(reader, 4)
		if err != nil {
			return nil, err
		}
		startPoint, err := Network.GetLocation(packageParts[1])
		if err != nil {
			return nil, err
		}
		endPoint, err := Network.GetLocation(packageParts[2])
		if err != nil {
			return nil, err
		}
		distance, err := strconv.Atoi(packageParts[3])
		if err != nil {
			return nil, err
		}

		packages = append(packages, &entities.Package{
			Name:       packageParts[0],
			StartPoint: startPoint,
			EndPoint:   endPoint,
			Distance:   distance,
		})
	}
	return packages, nil
}

func (c *CMD) GetPaths() ([]*entities.Path, error) {
	reader := bufio.NewReader(os.Stdin)
	numOfPaths, err := getNumber(reader)
	if err != nil {
		return nil, err
	}

	var paths []*entities.Path

	for i := 0; i < numOfPaths; i++ {
		pathParts, err := getDelimetedStrings(reader, 4)
		if err != nil {
			return nil, err
		}
		locationStart, err := Network.GetLocation(pathParts[1])
		if err != nil {
			return nil, err
		}
		locationEnd, err := Network.GetLocation(pathParts[2])
		if err != nil {
			return nil, err
		}
		distance, err := strconv.Atoi(pathParts[3])
		if err != nil {
			return nil, err
		}
		path := &entities.Path{
			Name:          pathParts[0],
			LocationStart: locationStart,
			LocationEnd:   locationEnd,
			Distance:      distance,
		}
		paths = append(paths, path)
	}
	return paths, nil
}

func getNumber(reader *bufio.Reader) (int, error) {
	numStr, err := reader.ReadString('\n')
	if err != nil {
		return 0, fmt.Errorf("err reading string %v", err)
	}
	numStr = strings.TrimSpace(numStr)
	num, err := strconv.Atoi(numStr)
	if err != nil {
		return 0, fmt.Errorf("not a number")
	}
	return num, nil
}

func getDelimetedStrings(reader *bufio.Reader, validationNum int) ([]string, error) {
	str, err := reader.ReadString('\n')
	if err != nil {
		return nil, fmt.Errorf("err reading string %v", err)
	}
	str = strings.TrimSpace(str)

	delimetedStrings := strings.Split(str, ",")
	for _, delimetedStr := range delimetedStrings {
		delimetedStr = strings.TrimSpace(delimetedStr)
	}
	if len(delimetedStrings) != validationNum {
		return nil, fmt.Errorf("invalid input, doesnt match count %v", validationNum)
	}
	return delimetedStrings, nil
}
