package server

import (
	"encoding/json"
	"os"
)

type config struct {
	ServerType string `json:"server_type"`
	Algorithm  string `json:"algorithm"`
}

var Config = config{}

func InitConfig() {
	configFile, err := os.Open("config.json")
	if err != nil {
		panic("unable to open config")
	}

	jsonParser := json.NewDecoder(configFile)
	if err = jsonParser.Decode(&Config); err != nil {
		panic("unable to parse config")
	}
}
